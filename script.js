// event ketika link di klik
$('.nav-item a').on('click', function(e) {
  // ambil isi href
  var ref = $(this).attr('href');
  //elemen yg tadi
  var elementRef = $(ref);
  $('html, body').animate({
    scrollTop:elementRef.offset().top-80
  }, 500);

  e.preventDefault();

});

//parallax
$(window).scroll(function() {
  let wScroll = $(this).scrollTop();

  $('.middle').css({
    'transform' : 'translate(0px, '+ wScroll/10 +'%)'
  });

  $('.jumbotron .overlay').css({
    'transform' : 'translate(0px, '+ wScroll/15 +'%)'
  });

  //Profile
  if (wScroll > $('.profile').offset().top-350) {
      $('.profile h1').addClass('muncul');
    }

  if (wScroll > $('.profile').offset().top-300) {
    $('.profile img').addClass('muncul');
  }

  if (wScroll > $('.profile').offset().top-200) {
    $('.profile p, .profile p i').addClass('muncul');
  }

});
